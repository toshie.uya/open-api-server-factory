const path = require("path")
const docheio = require("docheio")
const factory = require("../src")

const provider = new docheio.Provider()
const registry = new docheio.Registry(provider)
registry.register([
    {
        name: "getHealthCheck",
        singleton: false,
        factory: () => {
            return {
                execute: () => {
                    return {
                        meta: {
                            count: 1,
                            message: "Alive",
                            status: "OK",
                            code: 200
                        }
                    }
                }
            }
        }
    },
    {
        name: "getSample",
        singleton: false,
        factory: () => {
            return {
                execute: (sample, test) => {
                    console.log(sample)
                    console.log(test)
                    return {
                        meta: {
                            count: 1,
                            message: "Alive",
                            status: "OK",
                            code: 200
                        }
                    }
                }
            }
        }
    },
    {
        name: "samplePost",
        singleton: false,
        factory: () => {
            return {
                execute: ({path, body, header}) => {
                    console.log(header)
                    return {
                        meta: {
                            count: 1,
                            message: "Alive",
                            status: "OK",
                            code: 200
                        }
                    }
                }
            }
        }
    }
])

const executor = new factory.Executor(provider)
const sampleMiddleware = (req, res, next) => {
    console.log("Sample Middleware")
    next()
    console.log("Last process...")
}
const sampleMiddleware2 = (req, res, next) => {
    next()
    console.log("2nd Last process...")
}
const app = factory.serverFactory(
    path.join(__dirname, "openapi.yaml"),
    executor,
    (req) => {
        console.log("Running Sample Auth Check using Bearer Token Method")
        if (!req.header("Authorization")) {
            console.log("Access denied, token not found")
            throw {
                code: 401,
                message: "Unauthorized Access!"
            }
        }
    },
    [sampleMiddleware, sampleMiddleware2]
)

app.listen(3000, () => {
    console.log("Server is running")
})