declare namespace openApiServerFactory {
    interface IEndpoints {
        execute(...params: any[]): Promise<any>
    }

    function emitters(stream: string, type: string, data: any, meta: IEmitterMeta): Promise<void>

    interface IEmitterMeta {
        by: string
        at: Date
    }

    interface IWithHeader<T> {
        header: T
    }

    interface IWithPath<T> {
        path: T
    }
    
    interface IWithQuery<T> {
        query: T
    }

    interface IWithBody<T> {
        body: T
    }
}

export default openApiServerFactory
