const parseParam = (req) => {
  const parsed = {};
  
  req.openapi.schema.parameters.forEach((param) => {
    Object.assign(parsed, {
      [param.in]: {
        ...parsed[param.in],
        ...map[param.in](param, req),
      }
    })
  });

  if (req.openapi.schema.requestBody !== undefined) {
    parsed.body = req.body
  }

  return parsed;
}

const map = {
  path: (param, req) => {
    return {[param.name]: req.openapi.pathParams[param.name]}
  },
  query: (param, req) => {
    return {[param.name]: req.query[param.name]}
  },
  header: (param, req) => {
    return {[param.name]: req.header(param.name)}
  },
}

module.exports = parseParam
