const express = require("express")
const cookieParser = require("cookie-parser")
const cors = require("cors")
const { OpenApiValidator } = require("express-openapi-validator")
const openApiRouter = require("express-openapi-router")
const yamljs = require("yamljs")
const openApiHandler = require("./middlewares/openapi-handler")

module.exports = (apiPath, executor, authHandler, middlewares = []) => {
    const app = express()

    app.use(cors())
    app.use(express.json())
    app.use(express.urlencoded({ extended: false }))
    app.use(cookieParser())
    app.use('/spec', express.static(apiPath));

    middlewares.forEach(middleware => {
        app.use(middleware)
    })

    new OpenApiValidator({
        apiSpec: apiPath,
        validateRequests: true,
        validateResponses: false,
    }).install(app)
    
    const schema = yamljs.load(apiPath)
    const apiRoutes = new openApiRouter(schema)
    for (const key in schema.paths) {
        for (const method in schema.paths[key]) {
            const opSchema = schema.paths[key][method]
            const name = opSchema.operationId
            if (Array.isArray(opSchema.security) && opSchema.security.length > 0) {
                apiRoutes.use(name, async (req, res, next) => {
                    try {
                        await authHandler(req)
                        next()
                    } catch (e) {
                        res.status(e.code || 401).send({
                            message: e.message || "Not Authorized"
                        })
                    }
                })
            }
            apiRoutes.use(name, openApiHandler(executor, name))
        }
    }

    app.use("/v1",apiRoutes.router)

    app.use("*", (req, res, next) => {
        res.status(404).send({
            message: "Not Found"
        })
    })

    app.use((error, req, res, next) => {
        const errorResponse = error.error || error.errors || error.message || 'Unknown error';
        res.status(error.status || 500);
        res.type('json');
        res.json({ error: errorResponse });
    });

    return app
}
