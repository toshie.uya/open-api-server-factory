class Executor {
    constructor(provider) {
        this._provider = provider
    }

    async execute(service, params) {
        try {
            const op = this._provider.make(service)
            return op.execute(params)
        } catch (e) {
            throw {
                code: 500,
                message: "Unable to execute endpoint!"
            }
        }
    }
}

module.exports = Executor
