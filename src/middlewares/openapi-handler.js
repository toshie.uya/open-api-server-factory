const parseParam = require("../request-param-parser")
const _ = require("lodash")

module.exports = (executor, name) => {
    return async (req, res) => {
        try {
            const result = await executor.execute(name, parseParam(req))
            const code = _.get(result, "meta.statusCode", 200)
            res.status(code).send(result)
        } catch (e) {
            res.status(e.code || 500).send(e.data || {
                message: e.message
            })
        }
    }
}
